DROP DATABASE IF EXISTS `book_store`;
CREATE DATABASE `book_store`;

USE `book_store`;

CREATE TABLE `role` (
    `role_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `label` VARCHAR(50) NOT NULL
);

CREATE TABLE `user` (
    `user_id`   INT         AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `name`      VARCHAR(50),
    `email`     VARCHAR(50) UNIQUE,
    `password`  VARCHAR(255),
    `image`     VARCHAR(255),
    `role_id`   INT,
    FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`) ON DELETE RESTRICT
);

CREATE TABLE `delivery` (
    `delivery_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `user_id` INT NOT NULL,
    `address` VARCHAR(255) NULL,
    `index` INT NULL,
    `phone` VARCHAR(20) NULL,
     FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
);

CREATE TABLE `author` (
    `author_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `name` VARCHAR(100) NOT NULL
);

CREATE TABLE `genre` (
    `genre_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `label` VARCHAR(100) NOT NULL
);

CREATE TABLE `book` (
    `book_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `name` VARCHAR(100) NOT NULL,
    `author_id` INT NOT NULL,
    `description` VARCHAR(5000) NOT NULL,
    `price` INT NOT NULL,
    `count` INT NOT NULL,
    `date` DATETIME NOT NULL,
    FOREIGN KEY (`author_id`) REFERENCES `author` (`author_id`) ON DELETE RESTRICT
);


CREATE TABLE `book_images` (
    `image_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `book_id` INT NOT NULL,
    `path` VARCHAR(100) NOT NULL,
    FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON DELETE CASCADE
);

CREATE TABLE `book_genre` (
    `book_genre_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `book_id` INT NOT NULL,
    `genre_id` INT NOT NULL,
    FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON DELETE CASCADE,
    FOREIGN KEY (`genre_id`) REFERENCES `genre` (`genre_id`) ON DELETE CASCADE
);

CREATE TABLE `order` (
    `order_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `user_id` INT NOT NULL,
    `book_id` INT NOT NULL,
    `price` INT,
    `date` DATETIME NOT NULL,
    FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE,
    FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON DELETE RESTRICT
);

CREATE TABLE `comment` (
    `comment_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `book_id` INT,
    `user_id` INT NOT NULL,
    `content` VARCHAR(5000) NOT NULL,
    `date` DATETIME NOT NULL,
    FOREIGN KEY (`book_id`) REFERENCES `book` (`book_id`) ON DELETE CASCADE,
    FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE CASCADE
);

CREATE TABLE `contact` (
    `contact_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `address` VARCHAR(100) NOT NULL,
    `schedule` VARCHAR(100) NOT NULL,
    `phone` VARCHAR(20) NOT NULL,
    `google_lat` VARCHAR(30) NOT NULL,
    `google_ing` VARCHAR(30) NOT NULL
);

CREATE TABLE `new` (
    `new_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `title` VARCHAR(100) ,
    `content` VARCHAR(1000),
    `date` DATETIME,
    `image` VARCHAR(100)
);

CREATE TABLE `images` (
    `image_id` INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    `label` VARCHAR(30) NULL,
    `path` VARCHAR(100),
    href VARCHAR(100) NULL
);
