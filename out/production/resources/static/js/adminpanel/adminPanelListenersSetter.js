'use strict';
    var currentItem;

    var createModal = function(inner, isElement){
      var modal = document.createElement("div");
      modal.classList.add("modal-window");
      
      var container_ = document.createElement("div");
      container_.classList.add("modal-window__container");
      var closeFormBtn = document.createElement("div");
      closeFormBtn.classList.add("modal-window__close-form");
      container_.appendChild(closeFormBtn); 

      closeFormBtn.onclick = function(){
        closeModal();
      }

      var form_ = document.createElement("div");
      form_.classList.add("modal-window__form");

      modal.appendChild(container_);
      container_.appendChild(form_);
      if(isElement){
        form_.appendChild(inner);
      } else{
        form_.innerHTML = inner;
      }

      return modal;
    }

    var modal_;
    var closeModal = function() {
      if(modal_){
        // document.body.removeChild(modal_);
        modal_ = undefined;
      }
    }


    var currentForDeleteRec;
    var confirmDelete = function(){
      // alert(currentForDeleteRec);
      newAjaxRequest(currentForDeleteRec, function(result){
        if(result=="ok"){
          $('#deleteConfirmModal').modal('hide')
          onShowPage();
        }else{
          // alert(result);
          // onShowPage();
          // var container = document.createElement("div");
          // container.classList.add("modal-window__confirm-container");
          // var p = document.createElement("p");
          // p.innerHTML = result;
          // p.classList.add("modal-window__confirm-title")
          // container.appendChild(p);
          // modal_ =  createModal(container, true);
          // document.body.appendChild(modal_);
          $('#deleteConfirmModal').modal('hide')
          $('#deleteErrorDialog').modal('show')
          document.getElementById('deleteErrorDialog').querySelector('.modal-body').innerHTML = result;
        }
      });
    }
    var getDeleteItemCallback = function(item){
      return function(id){
        currentForDeleteRec = item['url'] + "/delete?id=" + id;

        // var container = document.createElement("div");
        // container.classList.add("modal-window__confirm-container");
        // var p = document.createElement("p");
        // setMessage("modal.areyousure", p)
        // p.classList.add("modal-window__confirm-title")

        // var btnContainer = document.createElement("div");
        // btnContainer.classList.add("modal-window__confirm-btn-container");

        // var btn = document.createElement("a");
        // btn.classList.add("modal-window__confirm-btn");
        // setMessage("modal.yes", btn)
        // btn.setAttribute("href", "#");
        // btn.onclick = function(){
          // newAjaxRequest(rec, function(result){
          //   if(result=="ok"){
          //     onShowPage();
          //   }else{
          //     onShowPage();
          //     var container = document.createElement("div");
          //     container.classList.add("modal-window__confirm-container");
          //     var p = document.createElement("p");
          //     p.innerHTML = result;
          //     p.classList.add("modal-window__confirm-title")
          //     container.appendChild(p);
          //     modal_ =  createModal(container, true);
          //     document.body.appendChild(modal_);
          //   }
          // });
        // }

        btnContainer.appendChild(btn);
        container.appendChild(p);
        container.appendChild(btnContainer);

        modal_ =  createModal(container, true);
        document.body.appendChild(modal_);
      }
    }

    var getShowPageCallback = function() {
        return function(id){
          var rec = currentItem['url'];
          if(id){
            rec += "/form/forexist?id="+id;
          } else {
            rec += "/form/fornew";
          }
          newAjaxRequest(rec, function(result){
            modal_ = createModal(result);
            var content_table   = document.querySelector(".content-table");
            content_table.innerHTML = "";
            content_table.appendChild(modal_);
            // document.body.appendChild(modal_);
          });
        }
    }

    var currentPage = 0;
    var onShowPage = function(page){
      closeModal();
      if(page != 0 && !page) {page = currentPage};
      currentPage = page;

        newAjaxRequest(currentItem['url'] + "/all/json?pageNum=" + page + "&countOnPage=" + 5 + "&content=part", function(result){
          var res_ = JSON.parse(result);
          showContent(res_.content, getShowPageCallback(currentItem), getDeleteItemCallback(currentItem));
          showPagination(res_);
        });
      }; 

    var setBtnListeners = (function () {
      
      return function (users, authors, books, contacts, news, gallery, add) {
        var _users=[], _authors=[], _books=[], _contacts=[], _news=[], _gallery=[], _add;
        _users['btn']    = document.querySelectorAll(users);
        _authors['btn']  = document.querySelectorAll(authors);
        _books['btn']    = document.querySelectorAll(books);
        _contacts['btn'] = document.querySelectorAll(contacts);
        _news['btn']     = document.querySelectorAll(news);
        _gallery['btn']  = document.querySelectorAll(gallery);
        _add             = document.querySelectorAll(add);

        _users['url']    = '/user';
        _authors['url']  = '/author';
        _books['url']    = '/book';
        _contacts['url'] = '/contact';
        _news['url']     = '/new';
        _gallery['url']  = '/gallery'; 

        currentItem = _authors;

        var items = [_users,_authors,_books,_contacts,_news,_gallery];


        var dellActive = function(){
          items.forEach(function (item, index) {
            if(item['btn']) {
              if(item['btn'][0]) item['btn'][0].classList.remove('active')
              if(item['btn'][0]) item['btn'][1].classList.remove('active')
            }
          })
        }

        items.forEach(function (item, index) {
          var onCl = function() {
            return function(){
              dellActive()
              this.classList.add('active');
              currentItem = item;
              onShowPage(0);
            }
          };
          if(item['btn']){
            if(item['btn'][0]) item['btn'][0].onclick = onCl();
            if(item['btn'][1]) item['btn'][1].onclick = onCl();
         }

         var addCl = function(){
          var f = getShowPageCallback("FFFF");
          f();
        };
          _add[0].onclick = addCl;
          _add[1].onclick = addCl;  

        });
      }
    }()); 

    var showAuthors = function(){
      onShowPage(0);
    }