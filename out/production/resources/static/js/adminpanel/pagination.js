'use strict';
    var showPagination = (function () {
      
      return function (pageable, prevCallback, nextCallback) {
        if(!pageable) return;

        var container  = document.querySelector(".table-pagination");
        var previous  = document.querySelector(".table-pagination__previous");
        var current   = document.querySelector(".table-pagination__current");
        var next      = document.querySelector(".table-pagination__next");
        
        var setDefault = function(){
          next.classList.remove("table-pagination__item_clicabl");
          previous.classList.remove("table-pagination__item_clicabl");

          next.classList.remove("table-pagination_vivible");
          previous.classList.remove("table-pagination_vivible");
          container.classList.remove("table-pagination_vivible");

          next.onclick = null;
          previous.onclick = null;
        }

        var onPrevious =  onPrevious = function(){
          if(prevCallback) prevCallback(pageable.pageable.pageNumber - 1)
            else onShowPage(pageable.pageable.pageNumber - 1)
        }
       
        var onNext = function(){
          if(nextCallback) nextCallback(pageable.pageable.pageNumber + 1)
          else onShowPage(pageable.pageable.pageNumber + 1)
        }
      

        setDefault();

        container.classList.add("table-pagination_vivible");

        if(!pageable.last){
          next.classList.add("table-pagination__item_clicabl");
          next.classList.add("table-pagination_vivible");
          next.onclick = onNext;
        } 
        if(!pageable.first){
          previous.classList.add("table-pagination__item_clicabl");
          previous.classList.add("table-pagination_vivible");
          previous.onclick = onPrevious;
        }
        current.innerHTML = pageable.pageable.pageNumber + 1;
        current.classList.add("table-pagination_vivible");
        
        // previous

        // var
          // _mainElement = document.querySelector(selector), // основный элемент блока
          // _sliderWrapper = _mainElement.querySelector(wraper), // обертка для .slider-item
          // _sliderItems = _mainElement.querySelectorAll(items), // элементы (.slider-item)
      }
    }());

    
