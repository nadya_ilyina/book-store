'use strict';
  var makeFieldEditable = (function () {

    return function (item, entityField, entityIdContainer, href, isPassword) {
      var
        container = document.querySelector(item),
        _field = container.querySelector('.account__value'),
        entityId = document.getElementById(entityIdContainer).value;

        var itemOnClick = function(){
          var oldValue = _field.innerHTML;
          _field.innerHTML = "";

          var input = document.createElement("input");
          if(!isPassword) input.value = oldValue;
          input.classList.add('account__value_editable')
          

          var buttonsContainer = document.createElement("div");
          buttonsContainer.classList.add('account__field-controll-container');
          var saveBtn = document.createElement("div");
          setMessage("account.control.save", saveBtn)

          saveBtn.classList.add('account__field-controll');
          var cancelBtn = document.createElement("div");
          setMessage("account.control.cancel", cancelBtn)

          cancelBtn.classList.add('account__field-controll');

          buttonsContainer.appendChild(cancelBtn);
          buttonsContainer.appendChild(saveBtn);

          _field.appendChild(input);
          _field.appendChild(buttonsContainer);

          container.onclick = null;
            
          var removeError = function(){
            container.classList.remove("account__value_editable_with_error");
          }

          var showError = function(error){
            // container.classList.add("account__value_editable_with_error");
            input.classList.add("account__value_editable_with_error");

            var cont = document.createElement("div");
            cont.classList.add('account__field-error-container')

            var errorContainer = document.createElement("div");
            errorContainer.classList.add('account__field-error-message');
            setMessage(error, errorContainer);

            cont.appendChild(errorContainer)
            _field.appendChild(cont); 

            input.focus();
          }

          cancelBtn.onclick = function(e){
            e.stopImmediatePropagation();
            _field.innerHTML=  oldValue
            container.onclick = itemOnClick;
            removeError()
          }

          saveBtn.onclick = function(e){
            e.stopImmediatePropagation();
            newAjaxRequest(href + "?id=" + entityId + "&" + entityField + "=" + input.value,
            function(result){
              if(!isPassword){
                _field.innerHTML=  input.value;
              } else {
                _field.innerHTML=  "***";
              } 
              container.onclick = itemOnClick;
              removeError()
            }, function(code, body){
              var res_;
              if(body && (res_ = JSON.parse(body))) showError(res_.errors[0]) 
              else  showError('error.default-user-enter-error');
            })
          }

          input.focus();
        }

        container.onclick = itemOnClick;
    }
  }());
