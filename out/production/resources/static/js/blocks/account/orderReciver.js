'use strict';
    var loadOrders = function(container, entityIdContainer){
      var entityId = document.getElementById(entityIdContainer).value;
      var itemsContainer = document.querySelector(container);

      var showContent = function(content){
        itemsContainer.innerHTML = "";
        content.forEach(element => {
          var order__item = document.createElement('div');
          order__item.classList.add('order__item');

          var divWrapper1 = document.createElement('div');
          divWrapper1.classList.add('sm-12'); divWrapper1.classList.add('md-2');
          divWrapper1.classList.add('lg-2');  divWrapper1.classList.add('xl-2');

          var order__img_wraper = document.createElement('div');
          order__img_wraper.classList.add('order__img-wraper');

          var order__img = document.createElement('img');
          order__img.classList.add('order__img');
          order__img.setAttribute('src', element.book.images[0].path)

          var divWrapper2 = document.createElement('div');
          divWrapper2.classList.add('sm-12'); divWrapper2.classList.add('md-10');
          divWrapper2.classList.add('lg-10'); divWrapper2.classList.add('xl-10');

          var order__name = document.createElement('div');
          order__name.classList.add('order__name');
          order__name.innerHTML = element.book.name;

          var divWrapper3 = document.createElement('div');
          divWrapper3.classList.add('sm-6'); divWrapper3.classList.add('md-6');
          divWrapper3.classList.add('lg-6'); divWrapper3.classList.add('xl-6');

          var order__prime = document.createElement('span');
          order__prime.classList.add('order__prime');
          order__prime.innerHTML = element.price;

          var order__prime_postfix = document.createElement('span');
          order__prime_postfix.classList.add('order__prime-postfix');
          order__prime_postfix.innerHTML =' USD';

          var divWrapper4 = document.createElement('div');
          divWrapper4.classList.add('sm-6'); divWrapper4.classList.add('md-6');
          divWrapper4.classList.add('lg-6'); divWrapper4.classList.add('xl-6');

          var order__date = document.createElement('div');
          order__date.classList.add('order__date');
          order__date.innerHTML = element.date;

          order__item.appendChild(divWrapper1);
          order__item.appendChild(divWrapper2);
          divWrapper1.appendChild(order__img_wraper);
          divWrapper2.appendChild(order__name);
          divWrapper2.appendChild(divWrapper3);
          divWrapper2.appendChild(divWrapper4);
          order__img_wraper.appendChild(order__img);
          divWrapper3.appendChild(order__prime);
          divWrapper3.appendChild(order__prime_postfix);
          divWrapper4.appendChild(order__date);
          
          itemsContainer.appendChild(order__item);

        })
      }

    var onShowPage = function(page){
      newAjaxRequest("/order/all/json?id="+entityId + "&pageNum=" + page + "&countOnPage=" + 3, function(result){
          var res_ = JSON.parse(result);
          showContent(res_.content);

          showPagination(res_, function(page){
            onShowPage(page);
          }, function(page){
            onShowPage(page);
          });
        });
      }; 
      onShowPage(0);
    }; 

