'use strict';
    var catalogConfig = (function () {

      var findByName = true;
      
      var filter = {};
      filter.genres = [];

      var _catalogItems;

      var showBooks = function(books, container){
        container.innerHTML = "";
        books.forEach(book => {
          var imageSrc = book.images[0].path;
          var bookId = book.id;
          var bookPrice = book.price;

          var bookAuthor = book.author.name;
          var bookName = book.name;

          var cont = document.createElement('div');
          cont.classList.add('catalog__item');
          cont.classList.add('sm-12');
          cont.classList.add('md-4');
          cont.classList.add('lg-3');
          cont.classList.add('xl-2');

          var book = document.createElement('div');
          book.classList.add('book');

          var bookLink = document.createElement('a');
          bookLink.classList.add('book__link');
          bookLink.setAttribute('href', '/book/card.html?id=' + bookId);

          var bookIconContainer = document.createElement('div');
          bookIconContainer.classList.add('book__icon-container');
          
          var bookIcon = document.createElement('img');
          bookIcon.classList.add('book__icon');
          bookIcon.setAttribute('src', imageSrc)

          var bookTextContainer = document.createElement('div');
          bookTextContainer.classList.add('book__text-container');

          var book__name = document.createElement('div');
          book__name.classList.add('book__name');
          book__name.innerHTML = bookName;

          var book_author = document.createElement('div');
          book_author.classList.add('book_author');
          book_author.innerHTML = bookAuthor;

          var book__price = document.createElement('div');
          book__price.classList.add('book__price');
          book__price.innerHTML = '$' + bookPrice;

          cont.appendChild(book);
          book.appendChild(bookLink);
          bookLink.appendChild(bookIconContainer);
          bookLink.appendChild(bookTextContainer);
          bookLink.appendChild(book__price);
          bookIconContainer.appendChild(bookIcon);
          bookTextContainer.appendChild(book__name)
          bookTextContainer.appendChild(book_author)
          
          container.appendChild(cont);
        })
      }

      function getPostParams(){
          var params = "";
          filter.genres.forEach(element => {
            params+='genres=' + element + "&"
          });
          if(findByName){
            var url_string = window.location.href; //window.location.href
            var url = new URL(url_string);
            var c = url.searchParams.get("text");
            if(c){
              params += 'text=' + c;
            }
         }
    
          return params;
      }

      var refrash = function(page = 0){
        newAjaxPostRequest("/book/all/json?pageNum=" + page, getPostParams() ,function(result){
          var res_ = JSON.parse(result);
          console.log(res_)
          showBooks(res_.content, _catalogItems);
          showPagination(res_, 
            function(page){
              refrash(page)
          }, function(page){
              refrash(page)
          });
        });
      }

      function arrayRemove(arr, value) {
        return arr.filter(function(ele){
            return ele != value;
        });
     }

     var createGanreItem = function(_id, _content, onChange_, checked_, classtype_ = 'catalog__genre'){
        var li = document.createElement('li');
        li.classList.add(classtype_);

        var label = document.createElement('label');
        label.classList.add('catalog_genre-label');

        var input = document.createElement('input');
        input.classList.add('catalog__genre-check-box');
        input.setAttribute('type', 'checkbox')
        input.setAttribute('value', _id);
        if(checked_)
          input.setAttribute('checked', checked_)

        var text = document.createElement('span');
        text.innerHTML = _content;

        li.appendChild(label);
        label.appendChild(input);
        label.appendChild(text);

        input.onchange = onChange_;

        return li;

     }

     var showFindByName = function(container){
      var url_string = window.location.href; //window.location.href
      var url = new URL(url_string);
      var searchText = url.searchParams.get("text");
      if(searchText){
        var li = createGanreItem("1", searchText, function(e){
          findByName = this.checked;
          refrash();
        }, true, 'catalog__genre_kook_name');

        container.appendChild(li);
      }
    }

      var showGenres = function(genres, container){
        container.innerHTML = "";
        genres.forEach(element => {
          var onChange = function(){
            if(this.checked){
              filter.genres.push(element.id)
            } else {
              filter.genres= arrayRemove(filter.genres, element.id, false);
            }
            refrash();
          }
          var li = createGanreItem(element.id, element.label, onChange)
          container.appendChild(li);
          
        });
       
      }      

      var loadGenres = function(container){
        newAjaxRequest("/genres/json", function(result){
          var res_ = JSON.parse(result);
          showGenres(res_, container);
          showFindByName(container);
        });
      }

      return function (container) {
        var
          _catalog = document.querySelector(container), 
          _catalogGenres = _catalog.querySelector('.catalog__genres');
          _catalogItems = _catalog.querySelector('.catalog__items');

          loadGenres(_catalogGenres);
          
          refrash();

      }
    }());
