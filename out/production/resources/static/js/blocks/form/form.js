'use strict';
    var setPasswordSchowAction = (function () {
        return function (selector) {
            var
             _mainElement = document.querySelector(selector),
             _controlHidePassword = _mainElement.querySelector('.form__hide-password'),
             _passwordInput = _mainElement.querySelector('.form__input');

             _controlHidePassword.onclick = function(){
                 var attr = _passwordInput.getAttribute('type');
                if(attr == 'text') {
                    _passwordInput.setAttribute('type', 'password');
                    setMessage('form.field.show', _controlHidePassword)
                }else {
                    _passwordInput.setAttribute('type', 'text')
                    setMessage('form.field.hide', _controlHidePassword)
                }
             }
        }
    })();