'use strict';
    var galleryConfig = (function () {
      // galleryRuner(".gallery");
        var _galleryContainer;

      var showNews = function(images, container){
        container.innerHTML = "";
        images.forEach(item => {
          var image = item.image;

          var cont = document.createElement('div');
          cont.classList.add('gallery__item');
          cont.classList.add('sm-12');
          cont.classList.add('md-6');
          cont.classList.add('lg-4');
          cont.classList.add('xl-3');

          var galleryContainer = document.createElement('div');
          galleryContainer.classList.add('gallery__container');

          galleryContainer.setAttribute('itemscope', '');
          galleryContainer.setAttribute('itemtype', "https://schema.org/ImageObject")
          
          var logo_url= document.createElement('meta');
          logo_url.setAttribute('itemprop', 'url')
          logo_url.setAttribute('content', image)
          galleryContainer.appendChild(logo_url);

          var gallery__img = document.createElement('img');
          gallery__img.classList.add('gallery__img');
          gallery__img.setAttribute('src', image);
          gallery__img.setAttribute('alt', 'Изображение галереи');
         

          cont.appendChild(galleryContainer);
          galleryContainer.appendChild(gallery__img);
          
          container.appendChild(cont);
        })
      }

      var loadNews = function(page = 0){
        newAjaxRequest("/gallery/all/json?countOnPage=12&pageNum=" + page, function(result){
          var res_ = JSON.parse(result);
          showNews(res_.content, _galleryContainer);
          showPagination(res_, 
            function(page){
              loadNews(page)
          }, function(page){
            loadNews(page)
          });
          galleryRuner(".gallery");
        });
      }

      
      return function (container) {
        _galleryContainer = document.querySelector(container);
        loadNews();
      }
    }());
