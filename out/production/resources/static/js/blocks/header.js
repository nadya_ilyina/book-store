'use strict';
    var setColorChangListener = (function () {
      
      return function (selectorRed, selectorGreen, selectorBlue) {
        var
          _colorbtnRed = document.querySelector(selectorRed),
          _colorbtnGreen = document.querySelector(selectorGreen),
          _colorbtnBlue = document.querySelector(selectorBlue);

          var _setClickListener = function(color){
            return function(){
              alert(color);
            }
          }
          _colorbtnRed.onclick = _setClickListener('red');
          _colorbtnGreen.onclick = _setClickListener('green');
          _colorbtnBlue.onclick = _setClickListener('blue');
      }
    }());
