var menuElem = document.getElementById('menu');
    var titleElem = menuElem.querySelector('.menu__schow-btn');
    var container = menuElem.querySelector('.menu__container');

    titleElem.onclick = function() {
        if(container.classList.contains('hiden')){
            container.classList.replace('hiden', 'visible');
        } else if(container.classList.contains('visible')){
            container.classList.replace('visible', 'hiden');
        } else {
            container.classList.add('visible');
        }
    };