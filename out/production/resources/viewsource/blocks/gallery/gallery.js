'use strict';

    var galleryRuner = (function () { 
      return function (selector) {
        var
          _mainElement = document.querySelector(selector), // основный элемент блока
          _galleryCloser = _mainElement.querySelector('.gallery__closer'), // обертка для .slider-item
          _galleryWrapper = _mainElement.querySelector('.gallery__wrapper'), // обертка для .slider-item
          __galleryItems = _mainElement.querySelectorAll('.gallery__item'), // элементы (.slider-item)
          __galleryControls = _mainElement.querySelectorAll('.gallery__control'), // элементы управления
          __galleryControlLeft = _mainElement.querySelector('.gallery__control_left'), // кнопка "LEFT"
          __galleryControlRight = _mainElement.querySelector('.gallery__control_right'), // кнопка "RIGHT"
          _wrapperWidth = parseFloat(getComputedStyle(_galleryWrapper).width), // ширина обёртки
          _wrapperHeight = parseFloat(getComputedStyle(_galleryWrapper).height), // выста обёртки
          _itemWidth = parseFloat(getComputedStyle(__galleryItems[0]).width), // ширина одного элемента    
          _positionLeftItem = 0, // позиция левого активного элемента
          _step = _itemWidth / _wrapperWidth * 100, // величина шага (для трансформации)
          _transform = 0, // значение транфсофрмации .slider_wrapper
          _items = []; // массив элементов
        // наполнение массива _items
        __galleryItems.forEach(function (item, index) {
          _items.push({ 
            item: item, 
            position: index, 
            transform: 0 , 
            container: item.querySelector('.gallery__container'),
            img: item.querySelector('.gallery__img')});
        });
        
        // значения по умолчанию
        //
        //__galleryControlRight.classList.add('gallery__control_show');
        //__galleryControlLeft.classList.remove('gallery__control_show');

        var position = {
          getMin: 0,
          getMax: _items.length - 1,
        }

        var _transformItem = function (direction) {
          if (direction === 'right') {
            if ((_positionLeftItem + _wrapperWidth / _itemWidth - 1) >= position.getMax) {
              return;
            }
            if (!__galleryControlLeft.classList.contains('gallery__control_show')) {
              __galleryControlLeft.classList.add('gallery__control_show');
            }
            if (__galleryControlRight.classList.contains('gallery__control_show') && (_positionLeftItem + _wrapperWidth / _itemWidth) >= position.getMax) {
              __galleryControlRight.classList.remove('gallery__control_show');
            }
            _positionLeftItem++;
            _transform -= _step;
          }
          if (direction === 'left') {
            if (_positionLeftItem <= position.getMin) {
              return;
            }
            if (!__galleryControlRight.classList.contains('gallery__control_show')) {
              __galleryControlRight.classList.add('gallery__control_show');
            }
            if (__galleryControlLeft.classList.contains('gallery__control_show') && _positionLeftItem - 1 <= position.getMin) {
              __galleryControlLeft.classList.remove('gallery__control_show');
            }
            _positionLeftItem--;
            _transform += _step;
          }
          _galleryWrapper.style.transform = 'translateX(' + _transform + '%)';
        }

        // обработчик события click для кнопок "назад" и "вперед"
        var _controlClick = function (e) {
          var direction = this.classList.contains('gallery__control_right') ? 'right' : 'left';
          e.preventDefault();
          _transformItem(direction);
        };

        var _openModal = function(i = 0){
          return function(){
            _mainElement.classList.add('gallery_modal');
            _galleryWrapper.classList.add('gallery__wrapper_modal');

            _items.forEach(function (item) {
              item.item.classList.add('gallery__item_modal');
              item.container.classList.add('gallery__container_modal');
            });
            
            if(i > 0){
              __galleryControlLeft.classList.add('gallery__control_show');
            }
            if(i + 1 < _items.length){
              __galleryControlRight.classList.add('gallery__control_show');
            }

            _wrapperWidth = parseFloat(getComputedStyle(_galleryWrapper).width);
            _wrapperHeight = parseFloat(getComputedStyle(_galleryWrapper).height);
            _itemWidth = parseFloat(getComputedStyle(__galleryItems[0]).width);  
            _positionLeftItem = 0;
            _step = _itemWidth / _wrapperWidth * 100;
            _transform = -_step*i;
            _positionLeftItem = i;
            _galleryWrapper.style.transform = 'translateX(' + _transform + '%)';
            
            _items.forEach(function (item) {
              var width = parseFloat(getComputedStyle(item.img).width);
              var height = parseFloat(getComputedStyle(item.img).height);
              var widthC = parseFloat(getComputedStyle(item.item).width);
              widthC = widthC < 1200 ? widthC : 1200;
              var heightC = parseFloat(getComputedStyle(item.item).height);
              var imgClass = (Math.abs(width - widthC) - Math.abs(height - heightC)) 
                ? 'gallery__img_tall' : 'gallery__img_wide';
              item.img.classList.add(imgClass);
            });
          }
        }

        var _closeModal = function(){
          _mainElement.classList.remove('gallery_modal');
          _galleryWrapper.classList.remove('gallery__wrapper_modal');
          
          _items.forEach(function (item) {
            item.item.classList.remove('gallery__item_modal');
            item.container.classList.remove('gallery__container_modal');
            item.img.classList.remove('gallery__img_tall');
            item.img.classList.remove('gallery__img_wide');
          });
          __galleryControls.forEach(function (item) {
            item.classList.remove('gallery__control_show');
          });
          _transform = 0;
          _galleryWrapper.style.transform = 'translateX(' + _transform + '%)';
        }

        var _setUpListeners = function () {
          // добавление к кнопкам "назад" и "вперед" обрботчика _controlClick для событя click
          __galleryControls.forEach(function (item) {
            //item.addEventListener('click', _controlClick);
            item.onclick = _controlClick;
          });

          _galleryCloser.onclick = _closeModal;

          var i = 0;
          _items.forEach(function (item) {
            item.item.onclick = _openModal(i++);
          });

        }

        // инициализация
        _setUpListeners();

        return {
          right: function () { // метод right
            _transformItem('right');
          },
          left: function () { // метод left
            _transformItem('left');
          }
        }

      }
    }());


