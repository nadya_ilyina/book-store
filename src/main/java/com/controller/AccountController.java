package com.controller;

import com.model.Delivery;
import com.model.User;
import com.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;

@Controller
public class AccountController {
    @Autowired
    UserRepository userRepository;


    @GetMapping(path = "/account.html")
    public String myAccount(Authentication authentication, Model model) {
        User user = (User) authentication.getPrincipal();
        user = userRepository.findById(user.id).get();

        if (user.delivery == null) user.delivery = new Delivery();
        if (user.orders == null) user.orders = new ArrayList<>();

        model.addAttribute("user", user);

        return "account";
    }
}
