package com.controller;

import com.model.Contact;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import com.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping(path="/contact")
public class ContactController {
    @Autowired
    private ContactRepository repository;


    @GetMapping("/form/forexist")
    public String getItem(@RequestParam Integer id, Model model){
        Contact contact = repository.findById(id).get();
        model.addAttribute("contact", contact);
        model.addAttribute("action", "/contact/update");
        return "modalwindow/contact/contact";
    }

    @GetMapping("/form/fornew")
    public String newItem(Model model){
        Contact contact = new Contact();
        model.addAttribute("contact", contact);
        model.addAttribute("action", "/contact/insert");
        return "modalwindow/contact/contact";
    }

    @PostMapping("/update")
    @ResponseBody
    public String update(@RequestParam Integer id,
                         @RequestParam String address,
                         @RequestParam String schedule,
                         @RequestParam String phone,
                         @RequestParam String googlelat,
                         @RequestParam String googleing){
        Contact contact = repository.findById(id).get();
        contact.address = address;
        contact.schedule = schedule;
        contact.phone = phone;
        contact.google_lat = googlelat;
        contact.google_ing = googleing;
        repository.save(contact);
        return "ok";
    }

    @PostMapping("/insert")
    @ResponseBody
    public String insert(@RequestParam String address,
                         @RequestParam String schedule,
                         @RequestParam String phone,
                         @RequestParam String googlelat,
                         @RequestParam String googleing){
        Contact contact = new Contact();
        contact.address = address;
        contact.schedule = schedule;
        contact.phone = phone;
        contact.google_lat = googlelat;
        contact.google_ing = googleing;

        repository.save(contact);

        return "ok";
    }


    @GetMapping(value = "/all/json", produces = "application/json")
    public @ResponseBody
    Iterable<Contact> adminPanel(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                                 @RequestParam(defaultValue = "50", required = false) Integer countOnPage){
        return repository.findAll(PageRequest.of(pageNum, countOnPage, new Sort("id")));
    }

    @GetMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam Integer id){
        try {
            repository.deleteById(id);
        } catch (Exception e){
            return e.toString();
        }

        return "ok";
    }

}
