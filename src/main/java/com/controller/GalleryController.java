package com.controller;

import com.model.ImageItem;
import com.repository.ImagesRepository;
import com.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping(path="/gallery")
public class GalleryController {


    @Autowired
    private ImagesRepository repository;

    @Autowired
    private StorageService storageService;

    @GetMapping("/form/forexist")
    public String getItem(@RequestParam Integer id, Model model){
        ImageItem imageItem = repository.findById(id).get();
        model.addAttribute("galleryItem", imageItem);
        model.addAttribute("action", "/gallery/update");
        return "modalwindow/gallery/gallery";
    }

    @GetMapping("/form/fornew")
    public String newItem(Model model){
        model.addAttribute("galleryItem", new ImageItem());
        model.addAttribute("action", "/gallery/insert");
        return "modalwindow/gallery/gallery";
    }

    @PostMapping("/update")
    @ResponseBody
    public String update(@RequestParam Integer id,
                         @RequestParam("image") MultipartFile imagePath){

        ImageItem item = repository.findById(id).get();

        if(!imagePath.isEmpty()){
            item.image = storageService.uploadFile(imagePath, String.valueOf(item.id), storageService.GALLERY);
        }

        repository.save(item);
        return "ok";
    }

    @PostMapping("/insert")
    @ResponseBody
    public String insert(@RequestParam("image") MultipartFile imagePath){
        System.out.println(imagePath != null);
        System.out.println("+=============================================");
        System.out.println(imagePath.isEmpty());

        ImageItem item = new ImageItem();
        item.label = "gallery";

        repository.save(item);

        item.id = repository.lastInsertedId();

        if(!imagePath.isEmpty()){
            item.image = storageService.uploadFile(imagePath, String.valueOf(item.id), storageService.GALLERY);
        }

        repository.save(item);
        return "ok";
    }

    @GetMapping(value = "/all/json", produces = "application/json")
    public @ResponseBody
    Iterable<ImageItem> adminPanel(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                                   @RequestParam(defaultValue = "10", required = false) Integer countOnPage,
                                   @RequestParam(defaultValue = "gallery", required = false) String label) {
        return repository.findByLabelOrderById(label, PageRequest.of(pageNum, countOnPage));
    }

    @GetMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam Integer id){
        repository.deleteById(id);
        return "ok";
    }

}