package com.controller;


import com.model.Book;
import com.model.Comment;
import com.model.ImageItem;
import com.model.User;
import com.repository.BookRepository;
import com.repository.CommentRepository;
import com.repository.ImagesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import sun.net.www.protocol.http.AuthenticationInfo;

import java.util.List;

@Controller
public class MainController {

    @Autowired
    private ImagesRepository repository;

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private CommentRepository commentRepository;

    @GetMapping(value = "/")
    public String root() {
        return "redirect:/index.html";
    }

    @GetMapping(value = "/index.html")
    public String index(Model model) {
        List<ImageItem> mainSlider = repository.findByLabel("mainSlider");
        model.addAttribute("mainslider", mainSlider);

        List<Book> books = bookRepository.findByOrderByDateDesc();
        if (books.size() > 6) books = books.subList(0, 6);
        model.addAttribute("books", books);

        List<Comment> comments = commentRepository.findByBookOrderByDateDesc(null);
        if (comments.size() > 3) comments = comments.subList(0, 3);
        model.addAttribute("comments", comments);

        return "index";
    }

    @GetMapping(value = "/delivery.html")
    public String delivery() {
        return "delivery";
    }

    @GetMapping(value = "/contacts.html")
    public String contacts() {
        return "contacts";
    }

    @GetMapping(value = "/news.html")
    public String news() {
        return "news";
    }

    @GetMapping(value = "/gallery.html")
    public String gallery() {
        return "gallery";
    }

    @GetMapping(value = "/sitemap.html")
    public String site_map() {
        return "site_map";
    }

    @GetMapping(value = "/cart.html")
    public String cart(Model model) {
        return "cart";
    }

    @GetMapping(value = "/buyForm.html")
    public String buyForm(Model model) {
        return "buyForm";
    }
}
