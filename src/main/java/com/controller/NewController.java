package com.controller;

import com.model.New;
import com.repository.NewRepository;
import com.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.sql.Date;

@Controller
@RequestMapping(path="/new")
public class NewController {
    @Autowired
    private NewRepository repository;

    @Autowired
    private StorageService storageService;

    @GetMapping("/get")
    public String get(@RequestParam Integer id, Model model) {
        New newItem = repository.findById(id).get();
//        newItem.content = newItem.content.
        model.addAttribute("newItem", newItem);
        return "/new";
    }

    @GetMapping("/form/forexist")
    public String getItem(@RequestParam Integer id, Model model){
        New newItem = repository.findById(id).get();
        model.addAttribute("newItem", newItem);
        model.addAttribute("action", "/new/update");
        return "modalwindow/new/new";
    }

    @GetMapping("/form/fornew")
    public String newItem(Model model){
        New newItem = new New();
        newItem.image = "/imgstore/new/default.jpeg";
        model.addAttribute("newItem", newItem);
        model.addAttribute("action", "/new/insert");
        return "modalwindow/new/new";
    }

    @PostMapping("/update")
    @ResponseBody
    public String update(@RequestParam Integer id,
                         @RequestParam String title,
                         @RequestParam String content,
                         @RequestParam("image") MultipartFile imagePath){

        New newItem = repository.findById(id).get();

        if(!imagePath.isEmpty()){
            newItem.image = storageService.uploadFile(imagePath, String.valueOf(newItem.id), storageService.NEW);
        }
        newItem.title = title;
        newItem.content = content.replaceAll("\n", "<br>");
        newItem.date = new Date(new java.util.Date().getTime());
        ;

        repository.save(newItem);
        return "ok";
    }

    @PostMapping("/insert")
    @ResponseBody
    public String insert(@RequestParam String title,
                         @RequestParam String content,
                         @RequestParam("image") MultipartFile imagePath){

        New newItem = new New();
        repository.save(newItem);

        newItem.id = repository.lastInsertedId();

        if(!imagePath.isEmpty()){
            newItem.image = storageService.uploadFile(imagePath, String.valueOf(newItem.id), storageService.NEW);
        }
        newItem.title = title;
        newItem.content = content.replaceAll("\n", "<br>");
        ;
        newItem.date = new Date(new java.util.Date().getTime());

        repository.save(newItem);
        return "ok";
    }

    @GetMapping(value = "/all/json", produces = "application/json")
    public @ResponseBody
    Iterable<New> adminPanel(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                             @RequestParam(defaultValue = "10", required = false) Integer countOnPage,
                             @RequestParam(defaultValue = "full", required = false) String content) {
        Page<New> news = repository.findByOrderById(PageRequest.of(pageNum, countOnPage));
        if (content.equals("part")) {
            for (New n : news.getContent()) {
                n.content = n.content.length() <= 10
                        ? n.content
                        : n.content.substring(0, 10) + "...";
            }
        }
        return news;
    }

    @GetMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam Integer id){
        repository.deleteById(id);
        return "ok";
    }

}