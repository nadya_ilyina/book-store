package com.controller;

import com.controller.book.tool.BookSercher;
import com.model.Book;
import com.model.Order;
import com.model.User;
import com.repository.BookRepository;
import com.repository.OrderRepository;
import com.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;


@Controller
public class ShoppingCart {

    @Autowired
    private BookRepository repository;

    @Autowired
    BookSercher bookSercher;
    /*@GetMapping("/cart.html")
    public String readCookie(HttpServletRequest req,@CookieValue(value = "bookId", defaultValue = "")  String cookieMas, Model model) {


        String mas_temp[] = cookieMas.split("/");
        List<Integer> list = new ArrayList<>();
        if(mas_temp.length > 0){
            for(String item : mas_temp){
                list.add(Integer.parseInt(item));
            }
            model.addAttribute("books",repository.findAllById(list));
        }
        return "cart";
    }*/

    @PostMapping(value = "/cart/all/json", produces = "application/json")
    @ResponseBody
    public Map<String, Long> postJson(@CookieValue(value = "bookId", defaultValue = "") String cookieMas) {
        return bookSercher.getBooksAsStringWithCountFromCookie(cookieMas);
    }

    @GetMapping("/writeCookie/{id:\\d+}")
    public String setCookie(HttpServletResponse response,
                            @CookieValue(value = "bookId", defaultValue = "") String cookieMas,
                            @PathVariable(name = "id", required = true) Integer id) {

        Cookie cookie;
        System.out.println(cookieMas);
        if (cookieMas.equals("")) {
            cookie = new Cookie("bookId", Integer.toString(id));
        } else {
            cookie = new Cookie("bookId", cookieMas + "/" + Integer.toString(id));
        }
        System.out.println(cookieMas);
        cookie.setMaxAge(1000 * 24 * 60 * 60);
        cookie.setPath("/");
        response.addCookie(cookie);

        return "redirect:/book/card.html?id=" + id;
    }

    @PostMapping("/cart/deleteBook/{id}")
    public String deleteCookie(HttpServletResponse response,
                               @CookieValue(value = "bookId", defaultValue = "") String cookieMas,
                               @PathVariable(name = "id", required = true) Long id) {

        String mas_temp[] = cookieMas.split("/");
        StringBuffer sb = new StringBuffer();
        if (mas_temp.length > 0) {
            for (String item : mas_temp) {
                if (!item.equals(id + "")) {
                    sb.append(item);
                    sb.append("/");
                }
            }
        }

        if (sb.length() > 0) {
            sb.setLength(sb.length() - 1);
        }

        Cookie cookie = new Cookie("bookId", sb.toString());
        cookie.setMaxAge(1000000 * 24 * 60 * 60);
        cookie.setPath("/");
        response.addCookie(cookie);

        return "redirect:/cart.html";
    }

    @PostMapping("/cart/changeCount/{id}/{value}")
    public ResponseEntity changeCookie(HttpServletResponse response,
                                       @CookieValue(value = "bookId", defaultValue = "") String cookieMas,
                                       @PathVariable(name = "id", required = true) Integer id,
                                       @PathVariable(name = "value", required = true) Long value) {
        Map<Integer, Long> map = bookSercher.getBooksForChangeCount(cookieMas);
        if (map.containsKey(id)) {
            map.replace(id, value);
        } else {
            map.put(id, value);
        }
        StringBuffer buf = new StringBuffer();
        for (Integer str : map.keySet()) {
            for (int i = 0; i < map.get(str); i++) {
                buf.append(str+"/");
            }
        }

        if (buf.length() > 0) {
            buf.setLength(buf.length() - 1);
        }

        Cookie cookie = new Cookie("bookId", buf.toString());
        cookie.setMaxAge(1000000 * 24 * 60 * 60);
        cookie.setPath("/");
        response.addCookie(cookie);
        return ResponseEntity.ok().build();
    }
    @Autowired
    UserRepository userRepository;

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    BookRepository bookRepository;

    @PostMapping("/buy/all/")
    public ResponseEntity buyAllBookFromCart( HttpServletResponse response,
                                              @CookieValue(value = "bookId", defaultValue = "") String cookieMas,
                                              Authentication authentication){
        User user = (User) authentication.getPrincipal();
        user = userRepository.findById(user.id).get();

        String mas_temp[] = cookieMas.split("/");

        for (String item : mas_temp) {
            Book book = bookRepository.findById(Integer.parseInt(item)).get();
            Order order = new Order();
            order.price = book.price;
            order.book = book;
            order.date = new Timestamp(new Date().getTime());
            order.user = user;
            try {
                orderRepository.save(order);
            } catch (Exception e) {
                e.printStackTrace();
                return ResponseEntity.badRequest().build();

            }
        }
        Cookie cookie;
        cookie = new Cookie("bookId", "");
        cookie.setMaxAge(1000 * 24 * 60 * 60);
        cookie.setPath("/");
        response.addCookie(cookie);
         return ResponseEntity.ok().build();

    }
}
