package com.controller.book;

import com.controller.book.tool.*;
import com.model.Book;
import com.model.User;
import com.repository.*;
import com.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

@Controller
@RequestMapping(path="/book")
public class BookController {
    @Autowired
    private BookRepository repository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookImagesRepository bookImagesRepository;

    @Autowired
    private StorageService storageService;

    @Autowired
    private BookGenreRepository bookGenreRepository;

    @Autowired
    ImagesUpdater imagesUpdater;
    @Autowired
    ImagesInserter imagesIserter;
    @Autowired
    GenreManager mGenreManager;
    @Autowired
    BookSercher bookSercher;
    @Autowired
    BookBuilder bookBuilder;
    @Autowired
    private GenreRepository genreRepository;

    @GetMapping("/form/forexist")
    public String getItem(@RequestParam Integer id, Model model){
        model.addAttribute("book", repository.findById(id).get());
        model.addAttribute("authors", authorRepository.findAll());
        model.addAttribute("images", bookImagesRepository.findByBookId(id));
        model.addAttribute("genres", genreRepository.findAll());
        model.addAttribute("action", "/book/update");
        return "modalwindow/book/book";
    }

    @GetMapping("/form/fornew")
    public String newItem(Model model){
        model.addAttribute("book", bookBuilder.build());
        model.addAttribute("authors", authorRepository.findAll());
        model.addAttribute("genres", genreRepository.findAll());
        model.addAttribute("action", "/book/insert");
        return "modalwindow/book/book";
    }

    @PostMapping("/update")
    @ResponseBody
    public String update(@RequestParam Integer id,
                         @RequestParam String name,
                         @RequestParam(name = "author") Integer authorId,
                         @RequestParam String description,
                         @RequestParam String price,
                         @RequestParam Integer count,
                         @RequestParam(required = false) Integer[] genre,
                         @RequestParam(name = "imagesNew") MultipartFile[] imagesNew,
                         @RequestParam(name = "imagesOldId", required = false) Integer[] imagesOldId,
                         @RequestParam(name = "deleteList", required = false) Integer[] deleteListId,
                         @RequestParam(name = "imagesAdded") MultipartFile[] imagesAdded){

        Book book = repository.findById(id).get();
        bookBuilder.build(book, authorId, count, description, name, price);
        imagesUpdater.update(book, imagesOldId, imagesNew, deleteListId, imagesAdded);

        repository.save(book);

        mGenreManager.update(book, genre);

        return "ok";
    }

    @PostMapping("/insert")
    @ResponseBody
    public String insert(@RequestParam String name,
                         @RequestParam(name = "author") Integer authorId,
                         @RequestParam String description,
                         @RequestParam String price,
                         @RequestParam Integer count,
                         @RequestParam(required = false) Integer[] genre,
                         @RequestParam(name = "imagesNew") MultipartFile[] imagesNew) {

        Book book = bookBuilder.build(authorId, count, description, name, price);
        repository.save(book);
        imagesIserter.insert(book, imagesNew);
        mGenreManager.insert(book, genre);
        return "ok";
    }


    @GetMapping(value = "/all/json", produces = "application/json")
    @ResponseBody
    public Iterable<Book> adminPanel(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                              @RequestParam(defaultValue = "50", required = false) Integer countOnPage){

        Page<Book> books = repository.findAll(PageRequest.of(pageNum, countOnPage, new Sort("id")));
        for(Book b: books.getContent()){
            b.description = b.description.length() <= 10
                    ? b.description
                    : b.description.substring(0, 10) + "...";
        }

        return books;
    }

    @PostMapping(value = "/all/json", produces = "application/json")
    @ResponseBody
    public Iterable<Book> postJson(@RequestParam(defaultValue = "0", required = false) Integer pageNum,
                                   @RequestParam(defaultValue = "15", required = false) Integer countOnPage,
                                   @RequestParam(name = "genres", required = false) Integer[] genresId,
                                   @RequestParam(name = "text", required = false) String text) {

        return bookSercher.search(pageNum, countOnPage, genresId, text);
    }

    @GetMapping(value = "/json", produces = "application/json")
    @ResponseBody
    public Book bookJson(@RequestParam Integer id) {
        return repository.findById(id).get();
    }

    @GetMapping(value = "/card.html")
    public String card(@RequestParam Integer id, Model model,
                       @RequestParam(required = false) String success,
                       @RequestParam(required = false) String error,

                       Authentication authentication) {
        User user;
        try {
            user = (User) authentication.getPrincipal();

        } catch (Exception e){
            user = new User();
        }
        model.addAttribute("user", user);

        Optional<Book> book = repository.findById(id);
        model.addAttribute("book", book.get());
        if (success != null) model.addAttribute("success", success);
        if (error != null) model.addAttribute("error", error);
        return "book";
    }

    @GetMapping("/delete")
    @ResponseBody
    public String delete(@RequestParam Integer id){
        repository.deleteById(id);
        return "ok";
    }

}
