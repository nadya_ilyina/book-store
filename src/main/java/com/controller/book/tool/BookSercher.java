package com.controller.book.tool;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.model.Author;
import com.model.Book;
import com.model.BookGenre;
import com.repository.AuthorRepository;
import com.repository.BookGenreRepository;
import com.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class BookSercher {

    @Autowired
    private BookRepository repository;

    @Autowired
    private AuthorRepository authorRepository;

    @Autowired
    private BookGenreRepository bookGenreRepository;

    @Autowired
    private ObjectMapper objectMapper;

    public Page<Book> search(Integer pageNum, Integer countOnPage, Integer[] genresId, String text) {
        Page<Book> books;
        if (genresId != null) {
            // Поиск книг по жанрам
            Iterable<BookGenre> genres = bookGenreRepository.findByGenreIdIn(Arrays.asList(genresId));

            ArrayList<Integer> bookIdList = new ArrayList<>();

            for (BookGenre genre : genres) {
                bookIdList.add(genre.book.id);
            }

            if (text != null) {
                Iterable<Author> authors = authorRepository.findByNameContaining(text);
                ArrayList<Integer> authorsIdList = new ArrayList<>();
                for (Author a : authors)
                    authorsIdList.add(a.id);

                books = repository.findByIdInAndNameContainingOrAuthorIdInOrderByDateDesc(
                        bookIdList, text, authorsIdList, PageRequest.of(pageNum, countOnPage));
            } else {
                books = repository.findByIdInOrderByDateDesc(bookIdList, PageRequest.of(pageNum, countOnPage));
            }

        } else {
            if (text != null) {
                Iterable<Author> authors = authorRepository.findByNameContaining(text);
                ArrayList<Integer> authorsIdList = new ArrayList<>();
                for (Author a : authors)
                    authorsIdList.add(a.id);

                books = repository.findByNameContainingOrAuthorIdInOrderByDateDesc(text, authorsIdList, PageRequest.of(pageNum, countOnPage));
            } else {
                books = repository.findByOrderByDateDesc(PageRequest.of(pageNum, countOnPage));
            }
        }
        return books;
    }

    public Page<Book> search(String cookieMas) {
        String mas_temp[] = cookieMas.split("/");
        List<Integer> list = new ArrayList<>();
        if (mas_temp.length > 0) {
            for (String item : mas_temp) {
                list.add(Integer.parseInt(item));
            }
            return repository.findByIdIn(list, PageRequest.of(0, list.size()));
        }
        return null;
    }

    public Map<String, Long> getBooksAsStringWithCountFromCookie(String cookieMas) {
        String bookIdsString[] = cookieMas.split("/");
        Map<Integer, Long> bookIdsWithCount = Stream.of(bookIdsString)
                .map(Integer::parseInt)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        List<Integer> bookIds = new ArrayList<>(bookIdsWithCount.keySet());
        if (bookIds.size() > 0) {
            Page<Book> books = repository.findByIdIn(bookIds, PageRequest.of(0, bookIds.size()));
            return books.stream().collect(Collectors.toMap(book -> {
                try {
                    return objectMapper.writeValueAsString(book);
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                    return null;
                }
            }, book -> bookIdsWithCount.get(book.id)));
        }

        return new HashMap<>();
    }

    public Map<Integer,Long> getBooksForChangeCount(String cookieMas){
        String bookIdsString[] = cookieMas.split("/");
        Map<Integer, Long> bookIdsWithCount = Stream.of(bookIdsString)
                .map(Integer::parseInt)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        return  bookIdsWithCount;
    }
}
