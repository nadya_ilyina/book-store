package com.controller.book.tool;

import com.model.Book;
import com.model.BookImage;
import com.repository.BookImagesRepository;
import com.storage.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@Component
public class ImagesUpdater {
    @Value("${book.img.default}")
    private String DEFAULT_IMAGE;

    @Autowired
    private BookImagesRepository bookImagesRepository;

    @Autowired
    private StorageService storageService;

    public void update(Book book,
                       Integer[] imagesOldId,
                       MultipartFile[] imagesNew,
                       Integer[] deleteListId,
                       MultipartFile[] imagesAdded) {

        List<BookImage> images = bookImagesRepository.findByBookId(book.id);
        HashMap<Integer, BookImage> imagesMap = new HashMap<>();
        for (BookImage img : images) {
            imagesMap.put(img.id, img);
        }

        if (imagesOldId != null) {
            for (int i = 0; i < imagesNew.length; i++) {
                if (!imagesNew[i].isEmpty()) {
                    if (imagesMap.containsKey(imagesOldId[i])) {
                        imagesMap.get(imagesOldId[i]).path
                                = storageService.uploadFile(imagesNew[i], book.id + "_" + i, storageService.BOOK);
                    }
                }
            }
        }

        if (deleteListId != null) {
            for (Integer i : deleteListId) {
                if (imagesMap.containsKey(i)) {
                    imagesMap.remove(i);
                    bookImagesRepository.deleteById(i);
                }
            }
        }

        images.clear();
        Collections.copy(Collections.singletonList(imagesMap.values()), images);

        int i = images.size();
        for (MultipartFile newImages : imagesAdded) {
            if (!newImages.isEmpty()) {
                BookImage bookImage = new BookImage();
                bookImage.book = book;
                bookImage.path = storageService.uploadFile(newImages, book.id + "_" + (i++), storageService.BOOK);
                images.add(bookImage);
            }
        }

        if (images.size() == 0 && bookImagesRepository.findByBookId(book.id).isEmpty()) {
            BookImage bookImage = new BookImage();
            bookImage.book = book;
            bookImage.path = DEFAULT_IMAGE;
            images.add(bookImage);
        }
        bookImagesRepository.saveAll(images);
    }

}
