package com.controller.service;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

@Controller
public class FileController {
    @Value("${upload.img}")
    private String path;

    @RequestMapping(value = "/${upload.img}/{file_dir}/{file_name}", method = RequestMethod.GET)
    public void getFile(
            @PathVariable("file_dir") String fileDir,
            @PathVariable("file_name") String fileName,
            HttpServletResponse response) {
        try {
            File file = new File(path + "/" + fileDir + "/" + fileName);
            InputStream is = new FileInputStream(file);
            IOUtils.copy(is, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException ex) {
            throw new RuntimeException("IOError writing file to output stream");
        }
    }
}
