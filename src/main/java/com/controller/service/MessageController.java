package com.controller.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MessageController {

    @Autowired
    private MessageSource messageSource;
    @RequestMapping(value = "/message/{message_name}", method = RequestMethod.GET)
    @ResponseBody
    public String getFile(@PathVariable("message_name") String messageName) {
        return messageSource.getMessage(messageName, null, LocaleContextHolder.getLocale());
    }

}
