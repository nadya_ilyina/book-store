package com.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "book")
public class Book {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "book_id")
    public Integer id;

    public String name;

    @ManyToOne(optional = false, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "author_id")
    public Author author;

    public String description;

    public Integer price;

    public Integer count;

    @JsonManagedReference
    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    public List<BookImage> images;

    @Temporal(TemporalType.DATE)
    public Date date;

    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    @JsonIgnore
    public List<BookGenre> genres;

    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    @JsonIgnore
    public List<Comment> comments;

    @JsonIgnore
    @OneToMany(mappedBy = "book", fetch = FetchType.LAZY)
    public List<Order> orders;

    public String getPrice() {
        return price == null ? "" : price / 100 + "." + price % 100;
    }

}
