package com.model;

import javax.persistence.*;

@Entity
@Table(name = "book_genre")
public class BookGenre {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "book_genre_id")
    public Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "book_id")
    public Book book;

    @ManyToOne(optional = false)
    @JoinColumn(name = "genre_id")
    public Genre genre;

}
