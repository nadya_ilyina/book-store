package com.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "comment")
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "comment_id")
    public Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "book_id", nullable = true)
    public Book book;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    public User user;

    public String content;

    //    @Temporal(TemporalType.TIMESTAMP)
    public Timestamp date;
}
