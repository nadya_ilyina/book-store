package com.model;

import javax.persistence.*;

@Entity
@Table(name = "contact")
public class Contact {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "contact_id")
    public Integer id;

    public String address;

    public String schedule;

    public String phone;

    public String google_lat;

    public String google_ing;

}
