package com.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "`order`")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id")
    public Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "book_id")
    public Book book;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id")
    public User user;

    public Integer price;

    public Timestamp date;

    public String getPrice() {
        return price == null ? "" : price / 100 + "." + price % 100;
    }

}
