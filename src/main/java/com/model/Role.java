package com.model;

import javax.persistence.*;

@Entity
@Table(name = "role")
public class Role {
    public static String ROLE_ADMIN = "ADMIN";
    public static String ROLE_MANAGER = "MANAGER";
    public static String ROLE_USER = "USER";

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "role_id")
    private Integer id;

    @Column(name = "label")
    private String label;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
