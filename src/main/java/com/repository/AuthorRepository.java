package com.repository;

import com.model.Author;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface AuthorRepository extends PagingAndSortingRepository<Author, Integer> {

    Iterable<Author> findByNameContaining(String text);

}
