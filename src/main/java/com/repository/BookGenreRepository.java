package com.repository;

import com.model.BookGenre;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;
import java.util.List;

public interface BookGenreRepository extends PagingAndSortingRepository<BookGenre, Integer> {
    List<BookGenre> findByGenreIdIn(Collection<Integer> list);


}