package com.repository;

import com.model.BookImage;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface BookImagesRepository extends PagingAndSortingRepository<BookImage, Integer> {
    List<BookImage> findByBookId(Integer bookId);
}
