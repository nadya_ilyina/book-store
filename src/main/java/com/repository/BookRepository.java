package com.repository;

import com.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Collection;
import java.util.List;

public interface BookRepository extends PagingAndSortingRepository<Book, Integer> {
    List<Book> findByOrderByDateDesc();

    Page<Book> findByOrderByDateDesc(Pageable pageable);

    Page<Book> findByNameContainingOrAuthorIdInOrderByDateDesc(String text, List<Integer> authorIds, Pageable pageable);

    Page<Book> findByIdInOrderByDateDesc(Collection<Integer> list, Pageable pageable);

    Page<Book> findByIdInAndNameContainingOrAuthorIdInOrderByDateDesc(Collection<Integer> list, String text, List<Integer> authorIds, Pageable pageable);

    Page<Book> findByIdIn(List<Integer> longId, Pageable pageable);



}