package com.repository;

import com.model.Contact;
import com.model.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ContactRepository extends PagingAndSortingRepository<Contact, Integer> {

}