package com.repository;

import com.model.Delivery;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DeliveryRepository extends PagingAndSortingRepository<Delivery, Integer> {
    public Delivery findByUserId(Integer userId);
}
