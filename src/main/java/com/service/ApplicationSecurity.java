package com.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;

@Configuration
@EnableWebSecurity
public class ApplicationSecurity extends WebSecurityConfigurerAdapter {

    private static String[] delete = {"login=true", "failed=true"};

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/index.html", "/login*", "/logout*", "/signup**",
                        "/perform_logout", "/css/**","/font/**","/vendor/**", "/js/**", "/img/**", "/imgstore/**",
                        "/message/**", "/sitemap*", "/robots.txt").permitAll()
                .antMatchers("/delivery.html**", "/news.html**", "/new/get**", "/contacts.html**","/cart.html","/writeCookie/**","/cart/**").permitAll()
                .antMatchers("/catalog.html**", "/book/all/json", "/book/json", "/book/card.html", "/genres/json**").permitAll()
                .antMatchers("/gallery.html**", "/gallery**", "/gallery/all/json").permitAll()
                .antMatchers("/new/all/json").permitAll()
                .antMatchers("/user/all/json", "/user**").access("hasRole('ADMIN')")
                .antMatchers("/admin-panel/**").access("hasRole('MANAGER')")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/index.html?login=true")
                .usernameParameter("email")
                .passwordParameter("password")
                .loginProcessingUrl("/login")
//                .defaultSuccessUrl("/index.html", true)
                .successHandler(successHandler())
                .failureUrl("/index.html?login=true&failed=true")
//                .failureHandler(errorHandler())
                .permitAll()
                .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/index.html");
    }

    @Bean
    public AuthenticationSuccessHandler successHandler() {
        SimpleUrlAuthenticationSuccessHandler handler = new SimpleUrlAuthenticationSuccessHandler();
        handler.setUseReferer(true);
        handler.setRedirectStrategy(new RedirectStrategy() {
            @Override
            public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
                for (String rem : delete) {
                    url = url.replaceAll(rem, "");
                }
                response.sendRedirect(url);
            }
        });
        return handler;
    }

    @Bean
    public AuthenticationFailureHandler errorHandler() {
        return new CustomAuthenticationFailureHandler();
//        SimpleUrlAuthenticationFailureHandler handler = new SimpleUrlAuthenticationFailureHandler();
////        handler.setUseForward(true);
//        System.out.println("FAILED!!!!!!!!!!!!!!!!!!!");

//        handler.setRedirectStrategy((request, response, url) -> {
//            if(url.contains("?")){
//                url+= "&login=true&failed=true";
//            } else {
//                url+= "?login=true&failed=true";
//            }
//            response.sendRedirect(url);
//        });
//        return handler;
    }

    public class CustomAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {

        @Override
        public void onAuthenticationFailure(
                HttpServletRequest request,
                HttpServletResponse response,
                AuthenticationException exception)
                throws IOException, ServletException {
            String url = null;

            HttpSession session = request.getSession();
            if (session != null) {
                url = (String) session.getAttribute("url_prior_login");
            }
            System.out.println(url);
            System.out.println("ssssssssssssssssssssssssssssssssssssssssss");
            assert session != null;
            Enumeration<String> dd = session.getAttributeNames();
            while (dd.hasMoreElements()) {
                System.out.println(dd.nextElement());
            }
            if (url == null) {
                url = "/index.html";
            }
            if (url.contains("?")) {
                url += "&login=true&failed=true";
            } else {
                url += "?login=true&failed=true";
            }

            response.sendRedirect(url);
        }
    }
}
