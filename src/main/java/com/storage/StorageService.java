package com.storage;

import com.exeption.StorageException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class StorageService {

    @Value("${upload.user}")
    public String USER = "user/";

    @Value("${upload.new}")
    public String NEW = "new/";

    @Value("${upload.gallery}")
    public String GALLERY = "gallery/";

    @Value("${upload.book}")
    public String BOOK = "book/";

    public String uploadFile(MultipartFile file, String filePrefix, String dir) {

        if (file.isEmpty()) {
            throw new StorageException("Failed to store empty file");
        }

        try {
            String fileName = filePrefix + file.getOriginalFilename();
            InputStream is = file.getInputStream();

            Files.copy(is, Paths.get(dir + fileName),
                    StandardCopyOption.REPLACE_EXISTING);

            return "/" + dir + fileName;

        } catch (IOException e) {

            String msg = String.format("Failed to store file", file.getName());

            throw new StorageException(msg, e);
        }

    }
}