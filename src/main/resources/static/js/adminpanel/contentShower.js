var showContent = (function () {
      
  return function (content, onClickCallback, onDeleteClickCallback) {

    addChild = function(container, content, withId){
      container.innerHTML = "";

      if(content instanceof Object){
        for (key in content) {
          if(key != "id"){
            container.innerHTML = container.innerHTML + content[key] + " "; 
          }
        }
      } else {
        container.innerHTML = content;
      }
    }

    var addChildImage = function(container, content){
      container.innerHTML = "";
      var image = document.createElement("img");
      image.setAttribute("src", content);
      image.classList.add("table__image-item");
      container.appendChild(image);
    }

    var content_table   = document.querySelector(".content-table");
    content_table.innerHTML = "";

    if(content.length > 0){

      //-----------COLUMNS NAME-------------
      var thead = document.createElement('thead');
      var tr = document.createElement('tr'); 
      content_table.appendChild(thead); 
      thead.appendChild(tr);

      var it = content[0];
      for (key in it) {
        var th = document.createElement('th');
        if(key != "id"){
            // th.classList.add('content-table_underline');
        }
        // th.classList.add("content-table__th");
        setMessage("adminpanel.table.title." + key, th)

        tr.appendChild(th);
      }
      tr.appendChild(document.createElement('th'));
      
      // -------------------------------------------
 
      // COLUMS CONTENT -------------------
      var tbody = document.createElement('tbody');
      content_table.appendChild(tbody);

      content.forEach(element => {
        var tr = document.createElement('tr');
        tbody.appendChild(tr);

        // tr.classList.add("content-table__line");

        for (key in element) {
          
          var td = document.createElement('td');
          if(key != "id"){
            td.classList.add('content-table_underline');
          }
          // td.classList.add("content-table__td");
          
          if(key == 'image'){
            addChildImage(td, element[key]);
          } else if(key == 'images'){
            if(element[key][0])
              addChildImage(td, element[key][0].path);
          } else {
            addChild(td, element[key], false);
          }
            
          td.onclick = function(){
            onClickCallback(element.id);
          } 

          tr.appendChild(td);
        };

        var td = document.createElement('td');
        td.classList.add("content-table__td");
        td.classList.add("content-table__delete-btn");
        td.onclick = function(){
          onDeleteClickCallback(element.id);
        }
        td.setAttribute('data-target',"#deleteConfirmModal")
        td.setAttribute('data-toggle',"modal")
        
        td.innerHTML="X";
        tr.appendChild(td);
        //---------------------------------------
      });
    } 
  }
}()); 