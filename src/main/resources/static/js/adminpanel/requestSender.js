function newAjaxRequest(addr, callback, error) {
  var method = "GET";
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 ) {
      if(this.status == 200){
        callback(this.responseText);
      } else {
        if(error)
          error(this.status, this.responseText)
      }
    }
  };
  xhttp.open(method, addr, true);
  xhttp.send(); 
}

function setMessage(mes, item) {
  newAjaxRequest("/message/" + mes, function(mes){
    item.innerHTML = mes;
  }, "GET");
}

function newAjaxPostRequest(addr, params, callback){
  var xhttp = new XMLHttpRequest();
  
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      callback(this.responseText);
    }
  };
  xhttp.open("POST", addr, true);
  xhttp.setRequestHeader( "Content-type","application/x-www-form-urlencoded" );
  xhttp.send(params); 
}