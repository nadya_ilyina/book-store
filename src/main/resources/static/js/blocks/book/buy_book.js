'use strict';


var
    _modalContainer,
    _controlCose,
    _controlSwitch_1,
    _controlSwitch_2,
    _form_1,
    _form_2;

var _switch_1 = function () {
    _controlSwitch_1.classList.add('modal__switcher-first_selected');
    _controlSwitch_2.classList.remove('modal__switcher-second_selected');
    _form_1.classList.add('modal__first_is-selected')
    _form_2.classList.remove('modal__second_is-selected');
}

var _switch_2 = function () {
    _controlSwitch_2.classList.add('modal__switcher-second_selected');
    _controlSwitch_1.classList.remove('modal__switcher-first_selected');
    _form_2.classList.add('modal__second_is-selected');
    _form_1.classList.remove('modal__first_is-selected');
}

var _openModal = function () {
    _modalContainer.classList.add('modal_is-visible');
}
var _closeModal = function () {
    _modalContainer.classList.remove('modal_is-visible');
}

var buyBook = function () {
    _openModal();
}

var configBuyBookModel = (function () {
    return function (selector) {
        _modalContainer = document.querySelector(selector),
            _controlCose = _modalContainer.querySelector('.modal__close-form'),
            _controlSwitch_1 = _modalContainer.querySelector('.modal__switcher-first'),
            _controlSwitch_2 = _modalContainer.querySelector('.modal__switcher-second'),
            _form_1 = _modalContainer.querySelector('.modal__first'),
            _form_2 = _modalContainer.querySelector('.modal__second');

        _controlSwitch_1.onclick = function (e) {
            _switch_1();
        };
        _controlSwitch_2.onclick = function (e) {
            _switch_2();
        };

        _controlCose.onclick = function (e) {
            _closeModal();
        };
    }
})();
