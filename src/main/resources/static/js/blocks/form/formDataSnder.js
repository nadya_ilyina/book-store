

var sendDataFromModalForm = function(selector, callback){
  var formElement = document.querySelector(selector);
  var addr = formElement.getAttribute("action");
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      if(!callback) onShowPage(); else callback(this.responseText);
    }
  };

  
  xhttp.open("POST", addr, true);
  xhttp.send(new FormData(formElement));

}  