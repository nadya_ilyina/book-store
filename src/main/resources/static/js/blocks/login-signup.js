'use strict';

    var
      _modalOpenLogin,
      _modalOpenSignup,
      _mainElement,
      _container,
      _controlCose,
      _controlSwitchLogin,
      _controlSwitchSignup,
      _loginForm,
      _signupForm;

    var _switchLogin = function (){
      _controlSwitchLogin.classList.add('user-modal__switcher-login_selected');
      _controlSwitchSignup.classList.remove('user-modal__switcher-signup_selected');
      _loginForm.classList.add('user-modal__login_is-selected')
      _signupForm.classList.remove('user-modal__signup_is-selected');
    }

    var _switchSignUp = function (){
      _controlSwitchSignup.classList.add('user-modal__switcher-signup_selected');
      _controlSwitchLogin.classList.remove('user-modal__switcher-login_selected');
      _signupForm.classList.add('user-modal__signup_is-selected');
      _loginForm.classList.remove('user-modal__login_is-selected');
    }

    var _openModalUser = function (){
      _mainElement.classList.add('user-modal_is-visible');  
    }
    var _closeModalUser = function (){
      _mainElement.classList.remove('user-modal_is-visible');  
    }
    var openSignUp = function() {
      _openModalUser();
      _switchSignUp();
    }
    var openLogIn = function(){
      _openModalUser();
    }

    var userModal = (function () {
        return function (selector, modalOpenLogin, modalOpenSignup) {
        
          _modalOpenLogin = document.querySelector(modalOpenLogin)
          _modalOpenSignup = document.querySelector(modalOpenSignup)
          _mainElement = document.querySelector(selector)
          _container = _mainElement.querySelector('.user-modal__container')
          _controlCose = _mainElement.querySelector('.user-modal__close-form')
          _controlSwitchLogin = _mainElement.querySelector('.user-modal__switcher-login')
          _controlSwitchSignup = _mainElement.querySelector('.user-modal__switcher-signup')
          _loginForm = _mainElement.querySelector('.user-modal__login')
          _signupForm = _mainElement.querySelector('.user-modal__signup')
        

            


             _controlSwitchLogin.onclick = function (e) {
                _switchLogin();
              };
              _controlSwitchSignup.onclick = function (e) {
                _switchSignUp();
              };

              if(_modalOpenLogin)
                _modalOpenLogin.onclick = function(e){
                    _openModalUser();
                    _switchLogin();
                }

              if(_modalOpenSignup)
             _modalOpenSignup.onclick = function(e){
                _openModalUser();
                _switchSignUp();
             }
             _controlCose.onclick = function (e) {
                _closeModalUser();
              };
        }
    })();